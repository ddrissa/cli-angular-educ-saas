import { CliAngular2EducSaasPage } from './app.po';

describe('cli-angular2-educ-saas App', () => {
  let page: CliAngular2EducSaasPage;

  beforeEach(() => {
    page = new CliAngular2EducSaasPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
